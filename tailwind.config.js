module.exports = {
  purge: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        orange: "#FFA500",
        mygray: "#c0c6d4",
        myblack: "#2c384f",
        mywhite: "#fdfdfd",
        backGray: "#f1f2f6",
      },
      spacing: {
        100: "37rem",
        39.6: "39.625rem",
      },
      outline: {
        blue: ["2px solid #3B82F6", "1px"],
      },
      maxWidth: {
        xs: "8rem",
      },
    },
  },
  variants: ["responsive", "group-hover", "hover", "focus", "active"],
  plugins: [],
};
