import React, { useState } from "react";
export const contextStore = React.createContext();

function ContextStoreProvider(props) {
  const [showEditBtn, setShowEditBtn] = useState(false);
  const [selectedSocial, setSelectedSocial] = useState([]);

  //states for link and websites block
  const [linkAndWebBlock, setLinkAndWebBlock] = useState(false);
  const [webTitle, setWebTitle] = useState("");
  const [webLink, setWebLink] = useState([{ title: "", link: "" }]);

  //state for social block
  const [socialBlock, setSocialBlock] = useState(false);
  const [socialTitle, setSocialTitle] = useState("");
  const [socialLink, setSocialLink] = useState({
    link1: "",
    link2: "",
    link3: "",
    link4: "",
    link5: "",
  });

  //state for aboutme block
  const [aboutBlock, setAboutBlock] = useState(false);
  const [aboutTitle, setAboutTitle] = useState("");
  const [aboutDescription, setAboutDescription] = useState("");

  //blur
  const [blur, setBlur] = useState(null);

  //contactBlock
  const [contactBlock, setContactBlock] = useState(false);
  const [contactTitle, setContactTitle] = useState("");
  const [contactDetails, setContactDetails] = useState({
    whatsapp: " ",
    phone: " ",
  });
  return (
    <>
      <contextStore.Provider
        value={{
          linkAndWebBlock,
          setLinkAndWebBlock,
          selectedSocial,
          setSelectedSocial,
          showEditBtn,
          setShowEditBtn,
          webTitle,
          setWebTitle,
          webLink,
          setWebLink,
          socialLink,
          setSocialLink,
          socialTitle,
          setSocialTitle,
          socialBlock,
          setSocialBlock,
          aboutBlock,
          setAboutBlock,
          aboutTitle,
          setAboutTitle,
          aboutDescription,
          setAboutDescription,
          setBlur,
          blur,
          contactDetails,
          setContactDetails,
          contactTitle,
          setContactTitle,
          contactBlock,
          setContactBlock,
        }}
      >
        {props.children}
      </contextStore.Provider>
    </>
  );
}

export default ContextStoreProvider;
