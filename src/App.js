import Home from "./Components/Home";
import BlockMain from "./Components/Blocks/BlockMain";
//mostusedblocks
import LinkAndWebsite from "./Components/Blocks/Mostusedblocks/LinkAndWebsite";
import Social from "./Components/Blocks/Mostusedblocks/Social";
import Aboutme from "./Components/Blocks/Mostusedblocks/Aboutme";
import SocialCreate from "./Components/Blocks/Mostusedblocks/SocialCreate";

// contactblock
import ContactCardBlock from "./Components/Blocks/Contactcardblocks/ContactCardBlock";

import "./App.css";
import { Route, Switch } from "react-router-dom";
import React, { useContext } from "react";
import { contextStore } from "./Contexts/ContextStoreProvider";

function App() {
  //states

  const { blur } = useContext(contextStore);

  return (
    <div className={`App ${blur ? "hide" : ""}`}>
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route exact path="/m">
          <BlockMain />
        </Route>
        <Route exact path="/m/linkandweb">
          <LinkAndWebsite />
        </Route>
        <Route exact path="/m/social">
          <Social />
        </Route>
        <Route exact path="/m/social/create">
          <SocialCreate />
        </Route>
        <Route exact path="/m/aboutme">
          <Aboutme />
        </Route>
        <Route exact path="/m/contactcard">
          <ContactCardBlock />
        </Route>
        <Route path="*">
          <h1>Page Not found</h1>
        </Route>
      </Switch>
    </div>
  );
}

export default App;
