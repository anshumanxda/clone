import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { BrowserRouter } from "react-router-dom";
import ContextStoreProvider from "./Contexts/ContextStoreProvider";
ReactDOM.render(
  <React.StrictMode>
    <ContextStoreProvider>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </ContextStoreProvider>
  </React.StrictMode>,
  document.getElementById("root")
);
