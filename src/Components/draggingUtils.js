import Content from "./Blocks/Content";
import AddLinkAndWeb from "./Blocks/Mostusedblocks/Add blocks/AddLinkAndWeb";
import AddSocial from "./Blocks/Mostusedblocks/Add blocks/AddSocial";
import AddAboutMe from "./Blocks/Mostusedblocks/Add blocks/AddAboutMe";
import AddContactDetail from "./Blocks/Contactcardblocks/Add block/AddContactDetail";

export const draggingUtils = [
  {
    id: "linkAndWeb",
    component: <AddLinkAndWeb />,
  },
  {
    id: "social",
    component: <AddSocial />,
  },
  {
    id: "aboutMe",
    component: <AddAboutMe />,
  },
  {
    id: "cotent1",
    component: <AddContactDetail />,
  },
  {
    id: "content2",
    component: <Content />,
  },
];
