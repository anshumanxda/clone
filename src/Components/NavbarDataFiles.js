import React from "react";
import { RiUserReceivedLine } from "react-icons/ri";
import { BiCopy } from "react-icons/bi";
import { BsBoxArrowInUpRight } from "react-icons/bs";
import { IoMailUnreadOutline } from "react-icons/io5";
import { FiHelpCircle, FiLogOut } from "react-icons/fi";
export const navbarDataFiles = [
  {
    icon: <RiUserReceivedLine className="text-blue-900 lg:text-3xl text-2xl" />,
    title: "My Dashboard",
    cname: "nav-text dashboard",
  },
  {
    icon: <BiCopy className="text-blue-900 lg:text-3xl text-2xl" />,
    title: "Copy URL",
    cname: "nav-text copy",
  },
  {
    icon: (
      <BsBoxArrowInUpRight className="text-blue-900 lg:text-3xl text-2xl" />
    ),
    title: "Upgrade",
    cname: "nav-text logout",
  },
  {
    icon: (
      <IoMailUnreadOutline className="text-blue-900 lg:text-3xl text-2xl" />
    ),
    title: "Support",
    cname: "nav-text logout",
  },
  {
    icon: <FiHelpCircle className="text-blue-900  lg:text-3xl text-2xl" />,
    title: "Whats StarBio",
    cname: "nav-text logout",
  },
  {
    icon: <FiLogOut className="text-blue-900 lg:text-3xl text-2xl" />,
    title: "Logout",
    cname: "nav-text logout",
  },
];
