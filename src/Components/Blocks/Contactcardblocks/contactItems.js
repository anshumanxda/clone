import { FiPhoneOutgoing } from "react-icons/fi";
import { RiWhatsappLine } from "react-icons/ri";

export const contactItems = [
  {
    name: "WhatsApp",
    icon: <RiWhatsappLine className="lg:text-3xl text-2xl  text-green-600" />,
  },
  {
    name: "Phone",
    icon: <FiPhoneOutgoing className="lg:text-3xl text-2xl  text-blue-600" />,
  },
];
