import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { contextStore } from "../../../Contexts/ContextStoreProvider";
import { contactItems } from "./contactItems";

function ContactCardBlock() {
  const {
    contactDetails,
    setContactDetails,
    contactTitle,
    setContactTitle,
    setContactBlock,
  } = useContext(contextStore);
  return (
    <>
      <div className="shadow-lg">
        <nav className=" flex justify-between lg:justify-around py-5 mx-5 lg:mx-0 ">
          <Link to="/m">
            <button className=" px-4 bg-gray-200 hover:bg-grey-700 text-gray-500 font-bold p-2 h-10 rounded-lg">
              Back
            </button>
          </Link>
          <Link
            onClick={setContactBlock(true)}
            to={contactTitle ? "/" : "/m/contactcard"}
          >
            <button className=" px-4 bg-blue-600 hover:bg-grey-700 text-white font-bold p-2 h-10 rounded-lg">
              Add Block
            </button>
          </Link>
        </nav>
      </div>

      <div className="container m-auto pt-10 max-w-full	px-5 lg:w-3/4 xl:px-60 2xl:px-80">
        <h1 className="text-2xl font-medium pb-3 antialiased	">Add a title</h1>
        <p className="pb-3 text-sm font-medium antialiased	">
          This title will appear above your content.
        </p>
        <input
          onChange={(e) => setContactTitle(e.target.value)}
          className=" appearance-none border-2 rounded-lg w-full py-4 px-3 text-gray-700 leading-tight focus:outline-none focus:border-blue-600	"
          placeholder="Add Your Title"
          type="text"
          value={contactTitle}
          id=""
        />

        {contactItems.map((item, index) => {
          return (
            <div
              key={index}
              className="container relative border-dotted rounded-lg	border-2 flex justify-center  items-start my-7 pb-4 px-2 pr-4 lg:py-5 lg:pb-8"
            >
              <div className="mt-6">{item.icon}</div>

              <input
                onChange={(e) => {
                  setContactDetails({
                    ...contactDetails,
                    [e.target.name]: e.target.value,
                  });
                }}
                className=" appearance-none text-sm lg:text-base border-2 self-center rounded-lg w-full mt-5 ml-5 py-5 px-6 text-gray-700 leading-tight focus:outline-none focus:border-blue-600	"
                placeholder={`Enter no. with country code without '+'`}
                type="tel"
                id="phone"
                name={index == 0 ? "whatsapp" : "phone"}
                value={
                  index == 0 ? contactDetails.whatsapp : contactDetails.phone
                }
              />
            </div>
          );
        })}
      </div>
    </>
  );
}

export default ContactCardBlock;
