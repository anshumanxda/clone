import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { contextStore } from "../../../../Contexts/ContextStoreProvider";
import { contactItems } from "../contactItems";
import Content from "../../Content";
//icons
import { RiPencilLine, RiDeleteBin6Line, RiWhatsappLine } from "react-icons/ri";
import { BiUserCircle } from "react-icons/bi";
import { FiPhoneOutgoing } from "react-icons/fi";

function AddContactDetail() {
  const {
    contactDetails,
    setContactDetails,
    contactTitle,
    setContactTitle,
    contactBlock,
    setContactBlock,
  } = useContext(contextStore);
  return (
    <>
      {contactBlock ? (
        <div className="flex mb-5 relative flex-col border-dashed border rounded-lg p-5 border-gray-300">
          <div>
            <div className="absolute flex text-xl lg:text-2xl text-blue-600 right-5 top-3">
              <Link to="/m/contactcard">
                <RiPencilLine className="mr-4 cursor-pointer" />
              </Link>
              <RiDeleteBin6Line
                onClick={() => {
                  setContactTitle("");
                  setContactDetails("");
                  setContactBlock(false);
                }}
                className=" cursor-pointer"
              />
            </div>

            <div className="head flex items-start">
              <BiUserCircle className="text-xl lg:text-2xl text-myblack" />
              <h4 className="ml-2 text-base lg:text-xl text-myblack font-semibold">
                {contactTitle}
              </h4>
            </div>
          </div>
          <div className="flex mx-5 mt-10 justify-around">
            <a
              className="whatsapp flex justify-center items-center h-14 w-28 rounded-lg bg-green-400"
              href={`https://wa.me/${contactDetails.whatsapp}`}
            >
              <RiWhatsappLine className="text-gray-50 text-2xl" />
            </a>
            <a
              className="phone flex justify-center items-center w-28 rounded-lg  bg-blue-400"
              href={`tel:+${contactDetails.phone}`}
            >
              <FiPhoneOutgoing className="text-gray-50 text-2xl" />
            </a>
          </div>
        </div>
      ) : (
        <Content />
      )}
    </>
  );
}

export default AddContactDetail;
