import React from "react";

function Box({ title, iconImage }) {
  return (
    <>
      <div className="flex text-myblack ml-5 flex-col pb-10">
        <div className="max-w-xs rounded-xl  max-h-xs bg-gray-300 ">
          <img className="rounded-xl" src={iconImage} alt={title} />
        </div>
        <h3 className="self-center pt-2 mt-2 text-sm lg:text-base font-semibold">
          {title}
        </h3>
      </div>
    </>
  );
}

export default Box;
