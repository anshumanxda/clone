import React, { useEffect, useRef, useContext } from "react";
import MostUsedNav from "./MostUsedNav";
import { SocialItem } from "./SocialItem";
//context
import { contextStore } from "../../../Contexts/ContextStoreProvider";
//toast
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

//actual function
const Social = () => {
  // usecontext
  const { setSelectedSocial, selectedSocial } = useContext(contextStore);
  const inputRef = useRef([]);
  const notify = () => {
    const customId = "custom-id-yes";
    toast.error("You can select only 5 items!!", {
      toastId: customId,
    });
  };
  const checkedOrNot = (e) => {
    if (e.target.checked) {
      setSelectedSocial((oldArray) => [...oldArray, e.target.id]);
      if (selectedSocial.length > 4) {
        notify();
      }
    } else {
      setSelectedSocial((item) => {
        return item.filter((val) => {
          return val !== e.target.id;
        });
      });
    }
  };

  const link = () => {
    return selectedSocial.length > 5 || selectedSocial.length < 1
      ? "/m/social"
      : "/m/social/create";
  };

  //store data in localstorage
  useEffect(() => {
    selectedSocial.map((item) => (inputRef.current[item].checked = true));
  }, []);

  return (
    <div>
      <ToastContainer
        autoClose={2000}
        draggablePercent={60}
        pauseOnFocusLoss={false}
      />
      <MostUsedNav btnName="Next" flink={link} />
      <div className="m-auto px-10 mt-5 lg:mt-10 lg:w-3/5 2xl:w-2/5 sm:w-full">
        <h1 className="text-xl text-gray-700 lg:pb-10 pb-5 font-semibold ">
          Select up to 5 social media platforms.
        </h1>
        <div>
          {SocialItem.map((item, index) => {
            return (
              <label key={index} className="select-none">
                <div className="flex mb-5 md:mb-5 lg:mb-0 lg:py-3 justify-between items-center lg:px-10 ">
                  <div className="icon flex items-center ">
                    {item.icon}
                    <h3 className="text-base font-normal lg:text-lg font-normal text-gray-800 pl-10">
                      {item.name}
                    </h3>
                  </div>
                  <input
                    onClick={checkedOrNot}
                    className="socialcheckbox h-4 w-4 rounded-full "
                    type="checkbox"
                    name={item.name}
                    id={index}
                    ref={(ref) => (inputRef.current[index] = ref)}
                  />
                </div>
              </label>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default Social;
