import React, { useContext } from "react";
import { RiDeleteBin6Line } from "react-icons/ri";
//context
import { contextStore } from "../../../Contexts/ContextStoreProvider";

function Links() {
  let { webLink, setWebLink } = useContext(contextStore);

  let handleChange = (i, e) => {
    let newWebLink = [...webLink];
    newWebLink[i][e.target.name] = e.target.value;
    setWebLink(newWebLink);
  };

  let addLinks = () => {
    setWebLink([...webLink, { title: "", link: "" }]);
  };

  let removeLinks = (i) => {
    let newWebLink = [...webLink];
    newWebLink.splice(i, 1);
    setWebLink(newWebLink);
  };
  return (
    <div>
      <h1 className="text-2xl pt-6 font-medium">Link</h1>
      {webLink.map((element, index) => (
        <div
          className="container relative border-dotted rounded-lg	border-2 flex justify-center flex-col items-center mt-5 py-10 px-10"
          key={index}
        >
          <RiDeleteBin6Line
            onClick={() => removeLinks(index)}
            className={`${
              index > 0 ? " " : "hidden "
            }cursor-pointer binIcon self-end mb-5 text-blue-600 text-3xl`}
          />
          <input
            onChange={(e) => handleChange(index, e)}
            className=" appearance-none border-2 rounded-lg w-full py-4 px-3 text-gray-700 leading-tight focus:outline-none focus:border-blue-600	"
            placeholder="Title"
            type="text"
            name="title"
            value={element.title}
          />
          <input
            onChange={(e) => handleChange(index, e)}
            className=" appearance-none border-2 self-center rounded-lg w-full mt-5 py-4 px-3 text-gray-700 leading-tight focus:outline-none focus:border-blue-600	"
            placeholder="URL"
            type="text"
            name="link"
            value={element.link}
          />
        </div>
      ))}
      <button
        onClick={() => addLinks()}
        className="px-3 mt-10 mb-10 bg-blue-600  items-center text-white font-bold py-4 h-13 rounded-lg"
      >
        Add More Link
      </button>
    </div>
  );
}

export default Links;
