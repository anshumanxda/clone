import React, { useContext } from "react";
import { Link } from "react-router-dom";
//context
import { contextStore } from "../../../Contexts/ContextStoreProvider";
import Links from "./Links";

//actual function
function LinkAndWebsite() {
  //state from contextStore
  let { setLinkAndWebBlock, webTitle, setWebTitle } = useContext(contextStore);
  return (
    <>
      <div className="shadow-lg">
        <nav className=" flex justify-between lg:justify-around py-5 mx-5 lg:mx-0 ">
          <Link to="/m">
            <button className=" px-4 bg-gray-200 hover:bg-grey-700 text-gray-500 font-bold p-2 h-10 rounded-lg">
              Back
            </button>
          </Link>
          <Link
            onClick={() => {
              if (webTitle) setLinkAndWebBlock(true);
            }}
            to={webTitle ? "/" : "/m/linkandweb"}
          >
            <button className=" px-4 bg-blue-600 hover:bg-grey-700 text-white font-bold p-2 h-10 rounded-lg">
              Add Block
            </button>
          </Link>
        </nav>
      </div>

      <div className="container m-auto  pt-10 w-3/4 lg:px-52 2xl:px-96 sm:px-5">
        <h1 className="text-2xl font-medium pb-3 antialiased	">Add a title</h1>
        <p className="pb-3 text-sm font-medium antialiased	">
          This title will appear above your content on your bio.
        </p>
        <input
          onChange={(e) => {
            setWebTitle((webTitle = e.target.value));
          }}
          className=" appearance-none border-2 rounded-lg w-full py-4 px-3 text-gray-700 leading-tight focus:outline-none focus:border-blue-600	"
          placeholder="Add Your Title"
          type="text"
          value={webTitle}
          id=""
        />
        <Links />
      </div>
    </>
  );
}

export default LinkAndWebsite;
