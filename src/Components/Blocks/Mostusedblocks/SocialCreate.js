import React, { useContext } from "react";
import { SocialItem } from "./SocialItem";
import { Link } from "react-router-dom";
//context
import { contextStore } from "../../../Contexts/ContextStoreProvider";

export default function SocialCreate() {
  // usecontext
  const {
    setSocialBlock,
    selectedSocial,
    socialLink,
    setSocialLink,
    setSocialTitle,
    socialTitle,
  } = useContext(contextStore);

  return (
    <div>
      <div className="shadow-lg">
        <nav className=" flex justify-between lg:justify-around py-5 mx-5 lg:mx-0 ">
          <Link to="/m">
            <button className=" px-4 bg-gray-200 hover:bg-grey-700 text-gray-500 font-bold p-2 h-10 rounded-lg">
              Back
            </button>
          </Link>
          <Link to="/">
            <button
              onClick={() => {
                setSocialBlock(true);
              }}
              className=" px-4 bg-blue-600 hover:bg-grey-700 text-white font-bold p-2 h-10 rounded-lg"
            >
              Add Block
            </button>
          </Link>
        </nav>
      </div>
      <div className="container m-auto pt-10 w-80 lg:w-3/4 xl:px-60 2xl:px-80 ">
        <h1 className="text-xl font-medium pb-3">Add a title</h1>
        <p className="pb-3 text-sm font-medium">
          This title will appear above your content on your bio.
        </p>
        <input
          onChange={(e) => {
            setSocialTitle(e.target.value);
          }}
          className=" appearance-none border-2 rounded-lg w-full py-5 mb-10 px-4 text-gray-700 leading-tight focus:outline-none focus:border-blue-600	"
          placeholder="Add Your Title"
          type="text"
          value={socialTitle}
        />
        {selectedSocial.map((item, index) => {
          return (
            <div
              key={index}
              className="container relative border-dotted rounded-lg	border-2 flex justify-center  items-start my-7 pb-4 px-2 pr-4 lg:py-5 lg:pb-8"
            >
              <div className="mt-6">{SocialItem[item].icon}</div>

              <input
                onChange={(e) => {
                  setSocialLink({
                    ...socialLink,
                    [e.target.name]: e.target.value,
                  });
                }}
                className=" appearance-none text-sm lg:text-base border-2 self-center rounded-lg w-full mt-5 ml-5 py-5 px-6 text-gray-700 leading-tight focus:outline-none focus:border-blue-600	"
                placeholder={`Add a valid ${SocialItem[item].name} username or URL`}
                type="text"
                name={`link${index + 1}`}
                value={socialLink["link" + (index + 1)]}
                id=""
              />
            </div>
          );
        })}
      </div>
    </div>
  );
}
