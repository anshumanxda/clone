import React, { useContext } from "react";
import { BiUserCircle } from "react-icons/bi";
import { RiDeleteBin6Line, RiPencilLine } from "react-icons/ri";
import { contextStore } from "../../../../Contexts/ContextStoreProvider";
import { chain as chainIcon } from "./chain";
import Content from "../../Content";
import { Link } from "react-router-dom";
//main function
function AddLinkAndWeb() {
  const {
    webTitle,
    setWebTitle,
    webLink,
    setWebLink,
    setLinkAndWebBlock,
    linkAndWebBlock,
    blur,
  } = useContext(contextStore);
  return (
    <>
      {linkAndWebBlock ? (
        <div className="flex mb-5 relative flex-col border-dashed border rounded-lg p-5 border-gray-300">
          <div className="absolute flex text-xl lg:text-2xl text-blue-600 right-5 top-3">
            <Link to="/m/linkandweb">
              <RiPencilLine className="mr-4 cursor-pointer" />
            </Link>
            <RiDeleteBin6Line
              onClick={() => {
                setWebTitle("");
                setWebLink([{ title: "", link: "" }]);
                setLinkAndWebBlock(false);
              }}
              className=" cursor-pointer"
            />
          </div>

          <div className="head flex">
            <BiUserCircle className="text-xl lg:text-2xl text-myblack" />
            <h4 className="ml-2 text-base lg:text-xl text-myblack font-semibold">
              {webTitle}
            </h4>
          </div>

          {webLink.map((element, index) => (
            <a href={`http://${element.link}`} key={index}>
              <div
                className={`download-section flex justify-between px-4 my-4 items-center rounded-lg ${
                  blur ? "" : "bg-backGray"
                } h-14`}
              >
                <p>{element.title}</p>
                {chainIcon}
              </div>
            </a>
          ))}
        </div>
      ) : (
        <Content />
      )}
    </>
  );
}

export default AddLinkAndWeb;
