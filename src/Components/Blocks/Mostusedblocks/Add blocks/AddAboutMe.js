import React, { useContext } from "react";
import parse from "html-react-parser";
import Content from "../../Content";
//icons
import { BiUserCircle } from "react-icons/bi";
import { RiDeleteBin6Line, RiPencilLine } from "react-icons/ri";
//context
import { contextStore } from "../../../../Contexts/ContextStoreProvider";
import { Link } from "react-router-dom";

function AddAboutMe() {
  const {
    aboutBlock,
    setAboutBlock,
    aboutTitle,
    setAboutTitle,
    aboutDescription,
    setAboutDescription,
  } = useContext(contextStore);
  return (
    <>
      {aboutBlock ? (
        <div className="flex mb-5 overflow-hidden relative flex-col border-dashed border rounded-lg p-5 border-gray-300">
          <div>
            <div className="absolute flex text-xl lg:text-2xl text-blue-600 right-5 top-3">
              <Link to="/m/aboutme">
                <RiPencilLine className="mr-4 cursor-pointer" />
              </Link>
              <RiDeleteBin6Line
                onClick={() => {
                  setAboutTitle("");
                  setAboutDescription("");
                  setAboutBlock(false);
                }}
                className=" cursor-pointer"
              />
            </div>
            <div className="head flex">
              <BiUserCircle className="text-xl lg:text-2xl text-myblack" />
              <h4 className="ml-2 text-base lg:text-xl text-myblack font-semibold">
                {aboutTitle}
              </h4>
            </div>
            <div className="mt-5 ml-2 flex items-baseline">
              <p className="text-myblack text-sm lg:text-base">
                {parse(aboutDescription)}
              </p>
            </div>
          </div>
        </div>
      ) : (
        <Content />
      )}
    </>
  );
}

export default AddAboutMe;
