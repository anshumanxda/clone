import React, { useContext } from "react";
import { BiUserCircle } from "react-icons/bi";
import { RiDeleteBin6Line, RiPencilLine } from "react-icons/ri";
import { contextStore } from "../../../../Contexts/ContextStoreProvider";
import { SocialItem } from "../SocialItem";
import Content from "../../Content";
import { Link } from "react-router-dom";

function AddSocial() {
  const {
    selectedSocial,
    setSelectedSocial,
    socialLink,
    setSocialLink,
    socialTitle,
    setSocialTitle,
    setSocialBlock,
    socialBlock,
  } = useContext(contextStore);

  const openInNewTab = (url) => {
    const newWindow = window.open(url, "_blank", "noopener,noreferrer");
    if (newWindow) newWindow.opener = null;
  };

  return (
    <>
      {socialBlock ? (
        <div className="flex mb-5 relative flex-col border-dashed border rounded-lg p-5 border-gray-300">
          <div>
            <div className="absolute flex text-xl lg:text-2xl text-blue-600 right-5 top-3">
              <Link to="/m/social/create">
                <RiPencilLine className="mr-4 cursor-pointer" />
              </Link>
              <RiDeleteBin6Line
                onClick={() => {
                  setSocialTitle("");
                  setSocialLink({
                    link1: "",
                    link2: "",
                    link3: "",
                    link4: "",
                    link5: "",
                  });
                  setSelectedSocial([]);
                  setSocialBlock(false);
                }}
                className=" cursor-pointer"
              />
            </div>

            <div className="head flex items-start">
              <BiUserCircle className="text-xl lg:text-2xl text-myblack" />
              <h4 className="ml-2 text-base lg:text-xl text-myblack font-semibold">
                {socialTitle}
              </h4>
            </div>
          </div>
          <div className="flex mx-5 mt-10 justify-between">
            {selectedSocial.map((item, index) => {
              return (
                <div
                  className="cursor-pointer"
                  key={index}
                  onClick={() => {
                    openInNewTab(`https://${socialLink["link" + (index + 1)]}`);
                  }}
                >
                  {SocialItem[item].icon}
                </div>
              );
            })}
          </div>
        </div>
      ) : (
        <Content />
      )}
    </>
  );
}

export default AddSocial;
