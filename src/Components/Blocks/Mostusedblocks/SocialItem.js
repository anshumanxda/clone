import React from "react";
import { AiOutlineInstagram, AiFillYoutube } from "react-icons/ai";
import { GrSoundcloud } from "react-icons/gr";
import { SiTiktok } from "react-icons/si";
import { RiSnapchatLine } from "react-icons/ri";
import {
  FaTwitter,
  FaFacebookF,
  FaMedium,
  FaTwitch,
  FaSpotify,
  FaLinkedinIn,
  FaApple,
  FaPatreon,
} from "react-icons/fa";
export const SocialItem = [
  {
    name: "Instagram",
    icon: (
      <AiOutlineInstagram className="lg:text-3xl text-2xl  text-pink-600" />
    ),
  },
  {
    name: "Twitter",
    icon: <FaTwitter className="lg:text-3xl text-2xl  text-blue-400" />,
  },
  {
    name: "Facebook Page",
    icon: <FaFacebookF className="lg:text-3xl text-2xl text-blue-700" />,
  },
  {
    name: "Youtube",
    icon: <AiFillYoutube className="lg:text-3xl text-2xl  text-red-500" />,
  },
  {
    name: "Medium",
    icon: <FaMedium className="lg:text-3xl text-2xl  text-gray-900" />,
  },
  {
    name: "Twitch",
    icon: <FaTwitch className="lg:text-3xl  text-2xl text-purple-700" />,
  },
  {
    name: "Spotify",
    icon: <FaSpotify className="lg:text-3xl  text-2xl text-green-500" />,
  },
  {
    name: "LinkedIn",
    icon: <FaLinkedinIn className="lg:text-3xl  text-2xl text-blue-600" />,
  },
  {
    name: "Soundcloud",
    icon: <GrSoundcloud className="lg:text-3xl  text-2xl text-orange" />,
  },
  {
    name: "Apple Music",
    icon: <FaApple className="lg:text-3xl  text-2xl text-gray-900" />,
  },
  {
    name: "Patreon",
    icon: <FaPatreon className="lg:text-3xl  text-2xl text-red-400" />,
  },
  {
    name: "Snapchat",
    icon: <RiSnapchatLine className="lg:text-3xl text-2xl  " />,
  },
  {
    name: "Tiktok",
    icon: <SiTiktok className="lg:text-3xl  text-2xl text-gray-800" />,
  },
];
