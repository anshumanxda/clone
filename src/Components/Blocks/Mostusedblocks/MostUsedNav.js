import React from "react";
import { Link } from "react-router-dom";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
function Nav(props) {
  const notify = () => {
    const customId = "custom-id-yes";
    toast.error("You can select only 5 items!!", {
      toastId: customId,
    });
  };

  const showToast = () => {
    if (props.flink() === "/m/social") {
      console.log(props.flink);
      notify();
    }
  };
  return (
    <>
      <ToastContainer
        autoClose={2000}
        draggablePercent={60}
        pauseOnFocusLoss={false}
      />
      <div className="shadow-lg">
        <nav className=" flex justify-between lg:justify-around py-5 mx-5 lg:mx-0 ">
          <Link to={props.blink ? props.blink : "/m"}>
            <button className=" px-4 bg-gray-200 hover:bg-grey-700 text-gray-500 font-bold p-2 h-10 rounded-lg">
              Back
            </button>
          </Link>
          <Link onClick={showToast} to={props.flink ? props.flink : "/m"}>
            <button className=" px-4 bg-blue-600 hover:bg-grey-700 text-white font-bold p-2 h-10 rounded-lg">
              {props.btnName ? props.btnName : "Add Block"}
            </button>
          </Link>
        </nav>
      </div>
    </>
  );
}

export default Nav;
