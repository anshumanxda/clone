import React, { useContext } from "react";
import { Link } from "react-router-dom";
import RichEditor from "./RichEditor";
import { contextStore } from "../../../Contexts/ContextStoreProvider";
function Aboutme() {
  const { setAboutBlock, aboutTitle, setAboutTitle } = useContext(contextStore);

  return (
    <div>
      <div className="shadow-lg">
        <nav className=" flex justify-between lg:justify-around py-5 mx-5 lg:mx-0 ">
          <Link to="/m">
            <button className=" px-4 bg-gray-200 hover:bg-grey-700 text-gray-500 font-bold p-2 h-10 rounded-lg">
              Back
            </button>
          </Link>
          <Link to="/">
            <button
              onClick={() => {
                setAboutBlock(true);
              }}
              className=" px-4 bg-blue-600 hover:bg-grey-700 text-white font-bold p-2 h-10 rounded-lg"
            >
              Add Block
            </button>
          </Link>
        </nav>
      </div>
      <div className="container flex m-auto items-center flex-col pt-10  w-4/5 lg:w-2/5 xl:w-4/5 xl:px-60 2xl:w-3/4 2xl:px-96 ">
        <h1 className="text-xl font-medium pb-3 self-start">Add a title</h1>
        <p className="pb-3 text-sm self-start">
          This title will appear above your content on your bio.
        </p>
        <input
          onChange={(e) => {
            setAboutTitle(e.target.value);
          }}
          className=" appearance-none mb-5 border-2 rounded-lg w-full py-4 px-3 text-gray-700 leading-tight focus:outline-none focus:border-blue-600	"
          placeholder="Add Your Title"
          type="text"
          value={aboutTitle}
          name=""
          id=""
        />
        <h1 className="self-start text-xl font-medium pb-3">
          Add A Description
        </h1>
        <p className="pb-3 text-sm md:self-start">
          Let people know who you are and what you care about! Describe yourself
          in 200 characters or less.
        </p>
        <RichEditor />
        <div className="mt-10"></div>
      </div>
    </div>
  );
}

export default Aboutme;
