import React, { useContext, useState } from "react";
import { EditorState, ContentState, convertFromHTML } from "draft-js";
import { Editor } from "react-draft-wysiwyg";
import { convertToRaw } from "draft-js";
import draftToHtml from "draftjs-to-html";
import "../../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { contextStore } from "../../../Contexts/ContextStoreProvider";

function RichEditor() {
  //state from contextStore
  let { aboutDescription, setAboutDescription } = useContext(contextStore);

  const [editorState, setEditorState] = useState(
    EditorState.createWithContent(
      ContentState.createFromBlockArray(convertFromHTML(aboutDescription))
    )
  );
  const onEditorStateChange = (editorState) => {
    setEditorState(editorState);
    setAboutDescription(
      draftToHtml(convertToRaw(editorState.getCurrentContent()))
    );
  };

  return (
    <div>
      <Editor
        defaultEditorState={editorState}
        wrapperClassName="demo-wrapper"
        editorClassName="demo-editor"
        onEditorStateChange={onEditorStateChange}
        placeholder="Add your description here"
        toolbar={{
          fontSize: { className: "demo-option-custom-medium" },
        }}
      />
    </div>
  );
}

export default RichEditor;
