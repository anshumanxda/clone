import React, { useContext } from "react";
import { Link } from "react-router-dom";
import { contextStore } from "../../Contexts/ContextStoreProvider";
function Content() {
  const { showEditBtn } = useContext(contextStore);
  return (
    <>
      {!showEditBtn ? (
        <Link to="/m">
          <div className="contentbox cursor-pointer h-52 lg:h-72 mb-5 flex justify-center items-center flex-col border-dashed border rounded-lg border-red-400">
            <h1 className="text-red-500 select-none text-lg font-semibold">
              TAP TO ADD CONTENT
            </h1>
          </div>
        </Link>
      ) : (
        ""
      )}
    </>
  );
}

export default Content;
