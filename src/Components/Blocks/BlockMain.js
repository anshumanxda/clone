import React from "react";
import { Link } from "react-router-dom";
import MostUsed from "./MostUsedCard";
import ContactCard from "./ContactCard";
function BlockMain() {
  return (
    <>
      <nav className="shadow-lg flex justify-between lg:block  py-5 lg:pl-0 px-5">
        <Link className="lg:px-96 2xl:px-100" to="/">
          <button className=" bg-gray-200 hover:bg-grey-700 text-gray-500 font-bold p-2 rounded-lg">
            Cancel
          </button>
        </Link>
      </nav>
      <div className="lg:px-96 2xl:px-100 px-8">
        <MostUsed />
        <ContactCard />
      </div>
    </>
  );
}

export default BlockMain;
