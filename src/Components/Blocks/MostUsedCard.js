import React, { useContext } from "react";
import Box from "./Box";
import { Link } from "react-router-dom";
import { contextStore } from "../../Contexts/ContextStoreProvider";
//svgs
import link from "./Svg/link.svg";
import description from "./Svg/description.svg";
import social from "./Svg/social.svg";

function MostUsed() {
  const { linkAndWebBlock, socialBlock } = useContext(contextStore);
  return (
    <div className="flex flex-col">
      <h1 className="py-10 font-semibold text-xl">MOST USED BLOCKS</h1>
      <div className="flex">
        <Link
          className={linkAndWebBlock ? "cursor-default" : ""}
          to={linkAndWebBlock ? "/m" : "/m/linkandweb"}
        >
          <Box title="Links & Websites" iconImage={link} />
        </Link>
        <Link
          className={socialBlock ? "cursor-default" : ""}
          to={socialBlock ? "/m" : "/m/social"}
        >
          <Box title="Social Icons" iconImage={social} />
        </Link>
        <Link to="/m/aboutme">
          <Box title="About Me" iconImage={description} />
        </Link>
      </div>
    </div>
  );
}

export default MostUsed;
