import React from "react";
import contact from "./Svg/contact.svg";
import { Link } from "react-router-dom";
import Box from "./Box";

function ContactCard() {
  return (
    <div className="flex flex-col">
      <h1 className="py-10 font-semibold text-xl">CONTACT BLOCK</h1>
      <div className="flex">
        <Link to="/m/contactcard">
          <Box title="Phone & WhatsApp" iconImage={contact} />
        </Link>
        <Link className="invisible" to="/m/contactcard">
          <Box title="Phone & WhatsApp" iconImage={contact} />
        </Link>
        <Link className="invisible" to="/m/contactcard">
          <Box title="Phone & WhatsApp" iconImage={contact} />
        </Link>
      </div>
    </div>
  );
}

export default ContactCard;
