import React, { useContext } from "react";
import Navbar from "./Navbar";
import { contextStore } from "../Contexts/ContextStoreProvider";
function Header() {
  const { showEditBtn, setShowEditBtn, blur } = useContext(contextStore);
  function showBtn() {
    setShowEditBtn(!showEditBtn);
  }
  return (
    <div
      className={`main-header shadow-md  z-10 flex fixed w-full justify-around sm:px-0 py-4 lg:py-7 lg:px-52 2xl:px-100 sm: ${
        blur ? "disableHeaderColor" : "bg-mywhite"
      }`}
    >
      <Navbar />
      <div
        className={`details flex flex-col justify-center items-center ${
          blur ? "filter blur-sm" : ""
        }`}
      >
        <img
          className="w-10 rounded-full"
          src="https://pbs.twimg.com/profile_images/879467646051332096/2yCm26qe_normal.jpg"
          alt=""
        />
        <h2 className="name text-lg text-myblack mt-5 lg:text-2xl font-bold">
          Anshumanxda
        </h2>
        <h6 className="link text-base text-mygray font-medium">
          bio.fm/anshumanxda
        </h6>
      </div>
      <div className={`button flex flex-col ${blur ? "filter blur-sm" : ""}`}>
        <button
          onClick={showBtn}
          className={`${
            showEditBtn ? "" : "hidden invisible"
          } bg-blue-500 hover:bg-blue-700 text-white font-bold px-3 p-2 h-10 text-sm rounded-lg`}
        >
          Edit block
        </button>
        <button
          className={`${
            showEditBtn ? "hidden invisible" : ""
          } mb-5 bg-blue-500 hover:bg-blue-700 text-white font-bold px-5 p-2 h-10 text-sm rounded-lg ${
            blur ? "disable-btn" : ""
          }`}
        >
          Publish
        </button>
        <button
          onClick={() => {
            setShowEditBtn(!showEditBtn);
          }}
          className={`${
            showEditBtn ? "hidden invisible" : ""
          } bg-gray-200 hover:bg-grey-700 text-blue-400 font-bold px-5 p-2 h-10 text-sm rounded-lg ${
            blur ? "disable-btn" : ""
          }`}
        >
          Cancel
        </button>
      </div>
    </div>
  );
}

export default Header;
