import Header from "./Header";
import Tooltip from "./Tooltip";

import { contextStore } from "../Contexts/ContextStoreProvider";
import { useContext, useState, useEffect } from "react";
//dragging
import { DragDropContext, Draggable, Droppable } from "react-beautiful-dnd";
import { draggingUtils } from "./draggingUtils";

function Home() {
  const [cards, updateCards] = useState(draggingUtils);
  const { blur } = useContext(contextStore);

  //drag function
  function handleOnDragEnd(result) {
    if (!result.destination) return;

    const items = Array.from(cards);
    const [reorderedItem] = items.splice(result.source.index, 1);
    items.splice(result.destination.index, 0, reorderedItem);

    updateCards(items);
  }

  //local storage
  useEffect(() => {
    const data = localStorage.getItem("my-drag-list");
    if (data) {
      updateCards(JSON.parse(data));
    }
  }, []);

  useEffect(() => {
    localStorage.setItem("my-drag-list", JSON.stringify(cards));
  });
  return (
    <>
      <Header />
      <div className={`${blur ? "filter blur-sm doGray" : ""} px-5`}>
        <DragDropContext onDragEnd={handleOnDragEnd}>
          <Droppable droppableId="droppable">
            {(provided, snapshot) => (
              <div
                {...provided.droppableProps}
                ref={provided.innerRef}
                className="relative top-40 md:top-48 flex items-center flex-col "
              >
                <div className="">
                  <Tooltip />
                  {cards.map((item, index) => {
                    return (
                      <Draggable
                        key={item.id}
                        draggableId={item.id}
                        index={index}
                      >
                        {(provided, snapshot) => (
                          <div
                            ref={provided.innerRef}
                            {...provided.draggableProps}
                            {...provided.dragHandleProps}
                          >
                            {item.component}
                          </div>
                        )}
                      </Draggable>
                    );
                  })}
                  {provided.placeholder}
                </div>
              </div>
            )}
          </Droppable>
        </DragDropContext>
      </div>
    </>
  );
}

export default Home;
