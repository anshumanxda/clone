import React, { useState, useContext } from "react";
import { navbarDataFiles as NavData } from "./NavbarDataFiles";
//icons
import { AiOutlineBars } from "react-icons/ai";
import { IoClose } from "react-icons/io5";
import { IoChevronForwardSharp } from "react-icons/io5";
//context
import { contextStore } from "../Contexts/ContextStoreProvider";

function Navbar() {
  const [navBar, setNavBar] = useState(false);
  const { setBlur, blur } = useContext(contextStore);

  const showNav = () => {
    setNavBar(!navBar);
    setBlur(!blur);
  };
  return (
    <>
      <div className=" navGray">
        <AiOutlineBars
          className="text-3xl text-blue-600 cursor-pointer"
          onClick={showNav}
        />
        {navBar ? (
          <div className="absolute left-1/2 -top-10 lg:top-10 ">
            <nav
              className={`bg-gray-50 navbarok rounded-lg text-gray-900 relative pt-20 px-10 -left-1/2 top-44 flex text-md ${
                blur ? "filter blur-none navblur" : ""
              } `}
            >
              <IoClose
                onClick={showNav}
                className="cursor-pointer relative text-2xl bottom-14 -right-60"
              />
              <div className="nav-items flex flex-col justify-start items-start blur-none">
                {NavData.map((item, index) => {
                  return (
                    <li
                      key={index}
                      className="{item.cname} flex justify-between  w-60 pb-10 list-none"
                    >
                      {item.icon}
                      <span className="text-lg font-medium text-gray-600 ">
                        {item.title}
                      </span>
                      <IoChevronForwardSharp className="text-blue-900 " />
                    </li>
                  );
                })}
              </div>
            </nav>
          </div>
        ) : (
          ""
        )}
      </div>
    </>
  );
}

export default Navbar;
