import React from "react";
//icon
import { BiCopy } from "react-icons/bi";
import { RiLightbulbFlashLine } from "react-icons/ri";
//toast
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

//comp. function
function Tooltip() {
  const notify = () => {
    const customId = "custom-id-yes";
    toast.success("Your username has been copied!!", {
      toastId: customId,
    });
    navigator.clipboard.writeText("anshumanxda");
  };
  return (
    <>
      <ToastContainer
        autoClose={2000}
        pauseOnFocusLoss={false}
        draggablePercent={60}
      />
      <div className="border-box flex flex-col justify-center w-96 lg:w-100 ">
        <div
          onClick={notify}
          className=" cursor-pointer copy transform transition duration-200 ease-in-out  lg:active:-translate-y-2  rounded-md mb-5 flex items-center p-5"
        >
          <div>
            <BiCopy className="text-3xl  text-blue-600" />
          </div>

          <div className=" pl-4 select-none">
            <h3 className="font-semibold">Get discovered!</h3>
            <p className="text-myblack text-sm font-medium mt-2">
              Copy your bio link and paste it in your profile to let people find
              you.
            </p>
          </div>
        </div>

        <a className="" href="mailto:biofm@gmail.com">
          <div className="mailus transform transition duration-150 ease-in-out  lg:active:-translate-y-2 mb-5 rounded-md flex items-center p-5">
            <div>
              <RiLightbulbFlashLine className="text-3xl text-blue-600" />
            </div>

            <div className=" pl-4 items-center">
              <h3 className="font-semibold">Tell us what you think</h3>
              <p className="text-myblack text-sm font-medium mt-2">
                Request a feature or suggest a change, let's make bio better
                together!
              </p>
            </div>
          </div>
        </a>
      </div>
    </>
  );
}

export default Tooltip;
